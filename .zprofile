EDITOR='vim'
#GPG_TTY=$(tty)
#GREP_COLOR='1;34'
GREP_OPTIONS='--color=auto'
LANG='en_GB.UTF-8'
PAGER='less'
PINENTRY='/usr/bin/pinentry-kwallet'
SSH_ASKPASS='/usr/bin/ksshaskpass'
VISUAL='vim'
export $EDITOR
#export $GPG_TTY
#export $GREP_COLOR
export $GREP_OPTIONS
export $LANG
export $PAGER
export $PINENTRY
export $SSH_ASKPASS
export $VISUAL
